import React, {Component} from 'react';
import './Banner.css';

class Banner extends Component {
  render() {
    return(
      <div className="banner">
        <div className="banner__container__left"><div className="banner__left"></div></div>
        <div className="banner__container__left--small"><div className="banner__left--small"></div></div>
          <div className="banner__main"><p className="banner__main__text">{this.props.title}</p></div>
          <div className="banner__container__right"><div className="banner__right"></div></div>
          <div className="banner__container__right--small"><div className="banner__right--small"></div></div>
      </div>
    );
  }
}

export default Banner;

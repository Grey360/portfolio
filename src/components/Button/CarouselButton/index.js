import React, {Component} from 'react';
import {Link} from 'react-scroll';
import './CarouselButton.css';

class CarouselButton extends Component {
  render() {
      const {onClick, rotate} = this.props;
      const classNameBtn = rotate ? 'carousel__button--rotate' : 'carousel__button';
    return(
      <Link
        activeClass="active"
        to='projets'
        spy={true}
        smooth={true}
        offset={0}
        duration= {500}
      >
        <div className={classNameBtn} onClick={onClick} >
            <div className="carousel__button2">
                <div className="carousel__button3">

                </div>
            </div>
        </div>
      </Link>
        
        
    );
  }
}

export default CarouselButton;

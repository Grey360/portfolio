import React, {Component} from 'react';
import {Link} from 'react-scroll';
import './Button.css';

class Button extends Component {
  render() {
    const name = this.props.name.charAt(0).toUpperCase() + this.props.name.slice(1);
    const isOnclickExist = this.props.onClick ? () => this.props.onClick() : '';
    return(
        <div className="button" >
          <Link
            onClick={isOnclickExist}
            activeClass="active"
            to={this.props.name}
            spy={true}
            smooth={true}
            offset={0}
            duration= {500}
          >
            <div  className="button__contain">
              <p className="button__contain__text">{name}</p>
            </div>
          </Link>
        </div>
    );
  }
}

export default Button;

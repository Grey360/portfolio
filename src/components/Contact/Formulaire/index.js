import React, {Component} from 'react';
import {Form, FormGroup, Input, Label, Button} from 'reactstrap';
import './Formulaire.css';

class Formulaire extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      message: '',
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange = e => {
    this.setState({[e.target.name]: e.target.value})
  }
   handleSubmit(e) {
    e.preventDefault();
    
  }
  
  render() {
    return(
      <form action="mailto:bousquet_yoan11@yahoo.fr" method="post" encType="text/plain">
        <input type="text" name="name"/>
        <input type="text" name="mail"/>
        <input type="text" name="comment" size="50"/>
        <input type="submit" value="Send"/>
        <input type="reset" value="Reset"/>
      </form>
    );
  }
}

export default Formulaire;

import React, {Component} from 'react';
import Formulaire from './Formulaire';
import imgMan from '../../images/personnage2.png';
import './Contact.css';

class Contact extends Component {
  render() {
    return(
      <div className="contact" id="contact">
        <div className="contact__container__text">
          <p className="skills__title">Contact</p>
        </div>
        <div className="contact__container">
          <div className="contact__container__man--img">
            <img src={imgMan} alt="man" className="contact__container__man--img1" />
          </div>
          <Formulaire />
        </div>
      </div>
    );
  }
}

export default Contact;

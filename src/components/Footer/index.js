import React, {Component} from 'react';
import linkedin from '../../images/linkedin.png';
import gitlab from '../../images/gitlab.png';
import './Footer.css';

class Footer extends Component {
  render() {
    return(
      <div className="footer" >
          <p className="footer__text">Bousquet yoan © 2020</p>
          <img src={linkedin} alt="linkedIn" className="footer__img" />
          <img src={gitlab} alt="gitlab" className="footer__img" />
          
      </div>
    );
  }
}

export default Footer;

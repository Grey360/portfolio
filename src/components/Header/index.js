import React, {Component} from 'react';
import ContainerButton from '../Landpage/ContainerButton';
import logo from '../../images/logo.png';

import './Header.css';

class Header extends Component {
  render() {
    return(
      <div className="header">
        <img src={logo} alt="logo" className="header__logo" />
        <ContainerButton onClick={this.props.onClick} />
      </div>
    );
  }
}

export default Header;

import React, {Component} from 'react';
import foot from '../../../images/foot.png';
import './Foot.css';

class Foot extends Component {
  render() {
    const {number, className, time} = this.props;
    const tabNumber = [];
    for (let i = 1; i <= number; i++) {
      tabNumber.push(i);
    }
    return(
      <div
        className={className}
      >
        {tabNumber.map(i => {
          const timer = (parseInt(time) + (i *100)).toString();
          if (i % 2 === 1) {
            return (<div data-aos="zoom-out"
                        data-aos-delay={timer}
                        data-aos-duration="500"
                        data-aos-offset="-100"
                        data-aos-once="true"
                        key={i}>
                      <img src={foot} alt="foot" className="foot__right"/>
                    </div>)
          } else {
            return (<div data-aos="zoom-out"
                          data-aos-delay={timer}
                          data-aos-duration="500"
                          data-aos-offset="-100"
                          data-aos-once="true"
                          key={i}>
                      <img src={foot} alt="foot" className="foot__left" />
                    </div>)
          }
        })}
      </div>
    );
  }
}

export default Foot;

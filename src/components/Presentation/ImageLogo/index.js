import React, {Component} from 'react';
import './ImageLogo.css';

class ImageLogo extends Component {
  render() {
      const {
        isNeverHover,
        classNames,
        classNameShadow,
        classNameImg,
        onMouseEnter,
        onMouseLeave,
        src,
		alt,
		delay,
		offset,
      } = this.props;
      const classNameNeverHover = (isNeverHover && alt === 'bac') ? 'object1--neverHover' : classNames;
    return(
        <div className={classNameNeverHover}>
            <div 
                data-aos="zoom-in"
                data-aos-delay={delay}
                data-aos-duration="500"
                data-aos-offset={offset}
                data-aos-once="true"
                onMouseEnter={(event) => onMouseEnter(event)}
                onMouseLeave={() => onMouseLeave()}
            >
                <img src={src} alt={alt} className={classNameImg} />
                <div className={classNameShadow}></div>
            </div>
        </div>
    );
  }
}

export default ImageLogo;

/*
<div 
    className="object1"
    data-aos="zoom-in"
    data-aos-delay="100"
    data-aos-duration="500"
    data-aos-once="true"
    onMouseEnter={(event) => this._onMouseEnter(event)}
    onMouseOut={() => this._onMouseOut()}
>
    <img src={imgBac} alt="bac" className="object1__img" />
    <div className={classNames[0]}></div>
</div>
    */

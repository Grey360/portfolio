import React, {Component} from 'react';
import './TextAbout.css';

class TextAbout extends Component {
  render() {
      const {className, number}  = this.props;
      console.log(number)
    return(
      <div className={className} data-aos="flip-left" data-aos-duration="700">
        <div className={className+'--small'}>
          
        </div>
        <p className="text__about__title">{data[number].title}</p>
        <p className="text__about__summarise">{data[number].summarise}</p>
        <div className={(number === 5 || number === 6 || number === 7) ? "container2__text__about__skill" : ''}>
          {data[number].skills.map(item => (
            <div className="container__text__about__skill" key={item}>
              <div className="text__about__skill__btn"></div>
              <p className="text__about__skill">{item}</p>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

const data = {
  1: {
    title: 'BAC',
    summarise: '2012 - Bac science de l\'ingénieur avec option mathématique.',
    skills: [],
  },
  2: {
    title: 'Saisonnier',
    summarise: '2013 à 2016 - Vente ambulante sur les plages méditerranéennes.',
    skills: ['Motivation', 'Esprit d\'équipe', 'Détermination'],
  },
  3: {
    title: 'Poker',
    summarise: '2015 et 2016 - Étude et analyse du Jeu. Élaboration d\'un programme d\'aide à la décision au poker.',
    skills: ['Créativité', 'Détermination', 'Passion'],
  },
  4: {
    title: 'Cuisine',
    summarise: '2018 - Commis de cuisine une année.',
    skills: ['Rigueur', 'Dicispline', 'Esprit d\'équipe'],
  },
  5: {
    title: 'Apprentissage',
    summarise: '2019 - Études en autonomie sur différentes technologies, JS, CSS et approfondissement du framework React et anglais technique.',
    skills: ['Autodidacte','Passion','Organisation', 'Discipline'],
  },
  6: {
    title: 'PICO',
    summarise: '2019 - Création d\'un projet de jeu de dès, élaboration d\'un cahier des charges et d\'une d\'application avec React & Redux.',
    skills: ['Détermination','Motivation', 'Créativité', 'Passion'],
  },
  7: {
    title: 'Formation',
    summarise: '2019 à 2021 - Formation avec l\'école OpenClassRoom dans le but d\'obtenir un dîplome BAC+3/4 de Développeur d\'application.',
    skills: ['Détermination', 'Engagement', 'Proactivité', 'Rigueur'],
  },
}

export default TextAbout;

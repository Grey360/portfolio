import React, {Component} from 'react';
import Carousel from 'nuka-carousel';
import ContainProject from '../ContainProject';
import CarouselButton from '../../Button/CarouselButton';
import CarouselDot from '../../Button/CarouselDot';
import './CarouselProjectPoker.css';


class CarouselProjectPoker extends Component {
  render() {
    const {projectFocus, handleClick, changeAfterSlide,slideIndex} = this.props;
    const arrayDot = [0,1,2,3];
    return(
          <div className="carousel__project__container">
            <Carousel 
              slideIndex={slideIndex} afterSlide={(slideIndex) => changeAfterSlide(slideIndex)} cellSpacing={20} 
              renderCenterLeftControls={({ previousSlide }) => {
                if (slideIndex !== 0) return <CarouselButton onClick={previousSlide} rotate={true} />
              }}
              renderCenterRightControls={({ nextSlide }) => {
                if (projectFocus === 'poker' && slideIndex !== 3) {
                  return <CarouselButton onClick={nextSlide} rotate={false} />
                }
              }}
              renderBottomCenterControls ={() => (
                <div className="carousel__project__container__dot">
                  {arrayDot.map(i => (
                    <CarouselDot onClick={handleClick} value={i} focus={slideIndex} key={i} />
                  ))}
                </div>
              )}
              >
                {
                  [1,2,3,4].map(i => {
                    if ( i === 1) return <ContainProject name='poker' display={1} page={i} key={i} />
                    if ( i === 2) return <ContainProject name='poker' display={4} page={i} key={i} />
                    if ((i === 3) || (i === 4)) return <ContainProject name='poker' display={3} page={i} key={i} />
                    return null;
                  }) }
            </Carousel>
          </div>
        
    );
  }
}

export default CarouselProjectPoker;

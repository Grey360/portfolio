import React, {Component} from 'react';
import Project from '../Project';
import './ContainerLittleProject.css';


class ContainerLittleProject extends Component {
  render() {
    const {projectFocus, onClick} = this.props;
    return(
        
        <div className="carousel__other__project__container">
            {['poker', 'pico', 'openclassroom'].map((project, index) => (
                <Project 
                  name={project} 
                  key={project} 
                  isLittle={true} 
                  onClick={onClick} 
                  projectFocus={projectFocus}
                  delay={index*200}
                />
            ))}
        </div>
        
    );
  }
}

export default ContainerLittleProject;
import React, {Component} from 'react';
import CarouselProjectOpenclassroom from '../CarouselProjectOpenclassroom';
import CarouselProjectPoker from '../CarouselProjectPoker';
import CarouselProjectPico from '../CarouselProjectPico'
import ContainerLittleProject from '../ContainerLittleProject';
import Project from '../Project';
import './ContainerProject.css';

class ContainerProject extends Component {
  render() {
    const {isCarousel, projectFocus, name, onClick, handleClick, slideIndex, changeAfterSlide,} = this.props;
    let carousel;
    if (name === 'pico') {
       carousel = <CarouselProjectPico
            projectFocus={projectFocus} 
            value={(Math.floor(Math.random() * Math.floor(20)))+1}
            onClick={onClick}
            handleClick={handleClick}
            slideIndex={slideIndex}
            changeAfterSlide={changeAfterSlide}
            /> 
    } else if (name === 'poker') {
      carousel = <CarouselProjectPoker
            projectFocus={projectFocus} 
            value={(Math.floor(Math.random() * Math.floor(20)))+1}
            onClick={onClick}
            handleClick={handleClick}
            slideIndex={slideIndex}
            changeAfterSlide={changeAfterSlide}
            />
    } else if (name === 'openclassroom') {
      carousel = <CarouselProjectOpenclassroom
            projectFocus={projectFocus} 
            value={(Math.floor(Math.random() * Math.floor(20)))+1}
            onClick={onClick}
            handleClick={handleClick}
            slideIndex={slideIndex}
            changeAfterSlide={changeAfterSlide}
            />
    }
    return(
      <div className="container__project">
        {(isCarousel) ? (
          <div className="carousel__project">
            <ContainerLittleProject projectFocus={projectFocus} onClick={onClick} />
            {carousel}
          </div>
        ) : (
          <div className="container__projects">
            {['poker', 'pico', 'openclassroom'].map((project, index) => (
              <Project onClick={onClick} name={project} delay={(index*200)} key={project} />
            ))}
          </div>
          )}
      </div>
    );
  }
}

export default ContainerProject;

import React, {Component} from 'react';
import './Poker.css';

class Poker extends Component {
  render() {
    const {onClick, value} = this.props;
    return(
      <div 
      className="poker__background"
      onClick={event => onClick(event, value)}
      >
        
      </div>
    );
  }
}

export default Poker;

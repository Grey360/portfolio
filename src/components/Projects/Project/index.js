import React, {Component} from 'react';
import {Link} from 'react-scroll';
import './Project.css';

class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {isHover: false};
  }
  _onMouseEnter = () => {
    this.setState({isHover: true});
  }
  _onMouseLeave = () => {
    this.setState({isHover: false});
  }
  render() {
    const {onClick, name, isLittle, projectFocus, delay} = this.props;
    const {isHover} = this.state;
    const contain = isLittle ? (
      <Link
      activeClass="active"
      to='projets'
      spy={true}
      smooth={true}
      offset={0}
      duration= {500}
    >
      <div
        data-aos="fade-up"
        data-aos-delay={delay}
        data-aos-duration="900"
        data-aos-once="true"
        data-aos-easing="ease-in-out-back"
        >
        <div 
          className="project--small" 
          style={projectFocus !== name ? {opacity: '0.3'}: {}}
          
          
          onClick={event => onClick(event, name)}
          onMouseEnter={() => this._onMouseEnter()}
          onMouseLeave={() => this._onMouseLeave()}
        >
          <div className={data[name].classNameImgLittle} style={isHover ? {transform: 'scale(1.1)'} :{} }>
          </div>
        </div>
      </div>
    </Link>
    ) : (
      <Link
        activeClass="active"
        to='projets'
        spy={true}
        smooth={true}
        offset={0}
        duration= {500}
      >
        <div 
          className="project"  
          data-aos="zoom-in"
          data-aos-delay={delay}
          data-aos-duration="900"
          data-aos-once="true"
          data-aos-easing="ease-in-out-back"
          onClick={event => onClick(event, name)}
          onMouseEnter={() => this._onMouseEnter()}
          onMouseLeave={() => this._onMouseLeave()}
        >
          <div className="project__background__hide">
            <div className="project__background__hide__text1" style={isHover ? {zIndex: 7} :{} }>
              <p className="project__background__hide__contain__text">
                {data[name].textBottom}
              </p>
            </div>
            <div className="project__background__hide__text2" style={isHover ? {zIndex: 7} :{} }>
              <p className="project__background__hide__contain__text">
                {data[name].textRight}
              </p>
            </div>
          </div>
          <div className={data[name].classNameImg} style={isHover ? {transform: 'scale(1.1)'} :{} }>
          </div>
        </div>
      </Link>
      
    );
    return(
      <div>{contain}</div>
    );
  }
}

const data = {
  'poker': {
    classNameImg: 'project__background--poker',
    classNameImgLittle: 'project__background--poker--little',
    textRight: 'Langage C',
    textBottom: 'Algorithme analyse Poker - 2015',
  },
  'pico': {
    classNameImg: 'project__background--pico',
    classNameImgLittle: 'project__background--pico--little',
    textRight: 'Javascript - React & Redux',
    textBottom: 'PICO - Jeu de dès - 2019',
  },
  'openclassroom': {
    classNameImg: 'project__background--openclassroom',
    classNameImgLittle: 'project__background--openclassroom--little',
    textRight: 'Développeur Frontend / Python',
    textBottom: 'Formation - 2020',
  },
}

export default Project;

import React, {Component} from 'react';
import ScrollTrigger from 'react-scroll-trigger';
import Counter from '../Counter';
import './Skill.css';

class Skill extends Component {
  constructor(props) {
    super(props);
    this.state = {visible: false};
  }
  onEnterViewport = () => {
    this.setState({visible: true,});
  }
  onExitViewport = () => {
    this.setState({visible: false});
  }
  render() {
    const {max, step, width, title} = this.props;
    const {visible} = this.state;
    const classNames = 'contain__skill' + width;
    return(
      <div className="skill" >
          <div className="skill__container">
          <ScrollTrigger
            onEnter={this.onEnterViewport}
            onExit={this.onExitViewport}
          >
            <div className={`${classNames} ${visible ? `${classNames}--animate` : ''}`}>
              {(visible && step) ? <Counter 
              max={max}
              step={step}
              />
            : <div className="container__skill__title"><p className="contain__skill__title">{title}</p></div>}
            </div>
          </ScrollTrigger>
            
          </div>
          
      </div>
    );
  }
}

export default Skill;
import React, {Component} from 'react';
import Skill from './Skill';
import imgCss from '../../images/css.png';
import imgHtml from '../../images/html.png';
import imgJs from '../../images/js.png';
import imgReact from '../../images/react.png';
import imgGit from '../../images/git.png';
import imgAnglais from '../../images/anglais.png';
import imgMan from '../../images/personnage.png';

import './Skills.css';


class Skills extends Component {
  render() {
    return(
      <div className="skills" id="compétences">
        <div className="skills__container">
          <p className="skills__title">Compétence</p>
        <div className="skill__container__man">
          <div className="skill__container__man--skill">
            {[0,1,2,3,4,5].map(item => (
              <div className="skill__container__skill" key={item}>
                <img src={skillData[item].src} alt="css" className={item !== 4 ? "skill__contain__img" : "skill__contain__img--git"} />
                <Skill
                  max={skillData[item].max} step={skillData[item].step} width={skillData[item].width}
                />
              </div>
            ))}
          </div>
          <div className="skill__container__man--img">
            {[1,2,3,4,5,6].map(item => (
              <img src={imgMan} alt="man" className="skill__container__man--img1"  key={item}/>
            ))}
          </div>
          <div className="skill__container__man--skill">
            {[6,7,8,9,10,11].map(item => (
              <div className="skill__container2__skill"  key={item}>
                <Skill
                  width={skillData[item].width} 
                  title={skillData[item].title}
                  max={skillData[item].max}
                  color="blue"

                />
              </div>
            ))}
          </div>
        </div>
        </div>
        
      </div>
    );
  }
}

const skillData = {
  0: {
    max: 68,
    src: imgCss,
    step: 25,
    width: '--204',
  },
  1: {
    max: 82,
    src: imgHtml,
    step: 21,
    width: '--246',
  },
  2: {
    max: 55,
    src: imgJs,
    step: 33,
    width: '--165',
  },
  3: {
    max: 73,
    src: imgReact,
    step: 23,
    width: '--219',
  },
  4: {
    max: 34,
    src: imgGit,
    step: 50,
    width: '--102',
  },
  5: {
    max: 63,
    src: imgAnglais,
    step: 25,
    width: '--189',
  },
  6: {
    max: 87,
    title: 'Curieux',//87 261
    width: '--261',
  },
  7: {
    max: 71,
    title: 'Créatif',//71 213
    width: '--213',
  },
  8: {
    max: 81,
    title: 'Autonome',//81 243
    width: '--243',
  },
  9: {
    max: 67,
    title: 'Rigoureux',//67 201
    width: '--201',
  },
  10: {
    max: 75,
    title: 'Coopératif',//75 225
    width: '--225',
  },
  11: {
    max: 91,
    title: 'Passioné',//91 273
    width: '--273',
  },
};


export default Skills;
